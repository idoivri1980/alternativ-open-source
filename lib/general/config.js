/**
 * Created by idoivri on 06/12/2015.
 */
DEBUG_APP = false; //turns on several debug

TLV_UNIVERSITY_GEOCOORDS = [32.105687,34.7997823];

//constants for calculations
DRIVING_PRICE_PER_KM_MULTIPLIER = 2.738/1000; //NIS
DRIVING_EMISSIONS_PER_KM_MULTIPLIER = 271/1000; //co2