/**
 * Created by idoivri on 23/11/2015.
 */

// Calc distance btwn two lan/lng points, using the Spherical Law of Cosines
// http://www.movable-type.co.uk/scripts/latlong.html
calcDistance = function(lat1,lon1,lat2,lon2){

    //console.log("calc distance: lat1="+lat1+", lon1="+lon1+" lat2="+lat2+", lon2="+lon2);

    var R = 6371; //earth's radius (KM)
    return Math.acos(Math.sin(_toRads(lat1))*Math.sin(_toRads(lat2)) +
            Math.cos(_toRads(lat1))*Math.cos(_toRads(lat2)) *
            Math.cos(_toRads(lon2-lon1))) * R;

}

//Helper: Convert numeric degrees into radians
function _toRads(num){
    return num * Math.PI / 180;
}

