App.info({
    id: 'io.zencity.alternativ',
    version: '2.0.1',
    name: 'AlterNativ',
    description: '',
    author: 'ZenCity',
    email: 'ido@zencity.io',
    website: 'http://zencity.io'
});
App.accessRule('*');
App.setPreference('cordova-background-geolocation-license', '******');
App.setPreference('cordova-background-geolocation-orderId', '******');
/*App.configurePlugin('com.phonegap.plugins.facebookconnect', {
    APP_ID: '450144881860156450144881860156',
    APP_NAME: 'YourFacebookAppName'
});*/

App.icons({
    //iOS
    'iphone': 'resources/icons-apple/icon-60x60.png',
    'iphone_2x': 'resources/icons-apple/icon-60x60@2x.png',
    'iphone_3x': 'resources/icons-apple/icon-60x60@3x.png',
    'ipad': 'resources/icons-apple/icon-76x76.png',
    'ipad_2x': 'resources/icons-apple/icon-76x76@2x.png',
    // Android
    'android_ldpi': 'resources/icons/icon-36x36.png',
    'android_mdpi': 'resources/icons/icon-48x48.png',
    'android_hdpi': 'resources/icons/icon-72x72.png',
    'android_xhdpi': 'resources/icons/icon-96x96.png'
});

App.launchScreens({
    // iOS
    'iphone': 'resources/splash-apple/splash-320x480.png',
    'iphone_2x': 'resources/splash-apple/splash-320x480@2x.png',
    'iphone5': 'resources/splash-apple/splash-320x568@2x.png',
    'iphone6': 'resources/splash-apple/splash-375x667@2x.png',
    'iphone6p_portrait': 'resources/splash-apple/splash-414x736@3x.png',
    'iphone6p_landscape': 'resources/splash-apple/splash-736x414@3x.png',

    // Android
    'android_ldpi_portrait': 'resources/splash/splash-200x320.png',
    'android_ldpi_landscape': 'resources/splash/splash-320x200.png',
    'android_mdpi_portrait': 'resources/splash/splash-320x480.png',
    'android_mdpi_landscape': 'resources/splash/splash-480x320.png',
    'android_hdpi_portrait': 'resources/splash/splash-480x800.png',
    'android_hdpi_landscape': 'resources/splash/splash-800x480.png',
    'android_xhdpi_portrait': 'resources/splash/splash-720x1280.png',
    'android_xhdpi_landscape': 'resources/splash/splash-1280x720.png'
});