if (Accounts.createUser && false) {
    if (Meteor.isServer) {
        var createTestUser = function () {
            return Accounts.createUser({
                username: 'testuser',
                email: 'test@zencity.io',
                password: '1234',
                profile: {
                    isTest: true
                }
            });
        };
        Meteor.methods({
            impersonate: function () {
                var testUser = Meteor.users.findOne({'profile.isTest': true});
                if (!testUser) {
                    var testUserId = createTestUser();
                    testUser = Meteor.users.findOne({_id: testUserId});
                }

                this.setUserId(testUser._id);
                return testUser._id;
            }
        });
    }

    if (Meteor.isClient) {
        Template._loginButtonsLoggedOut.onRendered(function () {
            $('#login-buttons').html('<a class="btn btn-primary" id="btn-impersonate">Sign in as Test User</a>');
            $('#btn-impersonate').click(function () {
                Meteor.call('impersonate', function (err, result) {
                    if (err) {
                        console.log(err);
                        return;
                    }
                    var userId = result;
                    Meteor.connection.setUserId(userId);
                    GeoTracker.updateFromUser(Meteor.user());
                    Session.set('isAdmin', true);
                    Router.go('navigate');
                });          
            });
        });
    }
}