Meteor.startup(function () {

    var facebookConfig = Meteor.settings.facebook;
    if (facebookConfig) {
        ServiceConfiguration = Package['service-configuration'].ServiceConfiguration;
        ServiceConfiguration.configurations.remove({
            service: "facebook"
        });
        ServiceConfiguration.configurations.insert({
            "service": "facebook",
            "appId": facebookConfig.appId,
            "secret": facebookConfig.secret,
            "loginStyle": "popup"
        });
    }
    
    Push.allow({
        send: function(userId, notification) {
            var user = Meteor.users.findOne({_id: userId});
            return SimplePermissions.isAdmin(user);
        }
    });


    if (Meteor.isServer) {

        dropboxStore = new FS.Store.Dropbox("routesExport", {
            key: "***",
            secret: "***",
            token: "***", // Don’t share your access token with anyone.
            //folder: "folder", //optional, which folder (key prefix) to use
            // The rest are generic store options supported by all storage adapters
            //transformWrite: myTransformWriteFunction, //optional
            //transformRead: myTransformReadFunction, //optional
            maxTries: 1 //optional, default 5
        });


        //add a FS object that allows writing exports by the CronJob below
        routesExport = new FS.Collection("routesExport", {
            //stores: [new FS.Store.FileSystem("routesExport", {path: "~/exports"})]
            stores: [dropboxStore]

        });

        routesExport.allow({
            'insert': function () {
                // no custom authentication needed, this is simply a server-side action
                return true;
            }
        });

        //timings based on later.js: https://bunkat.github.io/later/parsers.html#text

        SyncedCron.add({
            name: 'Export all data into fs every 1 days',
            schedule: function(parser) {
                // parser is a later.parse object
                return parser.text('at 02:30 am every 1 day');

            },
            job: function() {
                console.log("exporting data of the last 1 days to dropbox");
                var journeys = Meteor.call('exportJourneysbyPeriod', new Date(), 1, function(err, result) {
                    //console.log("Got result!", result);
                    if (err) {
                        console.log("Got error during export!",err);
                    }
                    else {

                        var data = JSON.stringify(result);
                        data = new Buffer(data);

                        var newFile = new FS.File();

                        newFile.attachData(data, {type: 'text/plain'}, function(error) {
                                if (error) throw error;
                                var date = new Date();
                                var dateStr = date.getFullYear()+"-"+date.getMonth()+"-"+date.getDay()+"-"+date.getHours()+"-"+date.getMinutes();
                                newFile.name(dateStr+'.json');

                                routesExport.insert(newFile);

                            });
                    }

                });
            }
        });

        SyncedCron.start();

    }
});