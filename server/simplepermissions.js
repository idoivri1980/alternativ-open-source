/* 
 * Hard-coded permissions
 */

SimplePermissions = {
    _adminFacebookUserIds: [
        ''
    ],
    _adminEmails: [
        'amir.ozer@gmail.com',
        'dalyot@tx.technion.ac.il',
        'idoivri@gmail.com',
        'amir@zencity.io'
    ],
    isAdmin: function (user) {
        return user && (
                (user.services.facebook && this._adminFacebookUserIds.indexOf(user.services.facebook.id) > -1) ||
                (user.emails && this._adminEmails.indexOf(user.emails[0].address) > -1) ||
                (user.profile && (user.profile.name === 'Ido Ivri')) ||
                (user.profile && user.profile.isTest)
            );
    }
};

Meteor.methods({
    isAdmin: function () {
        return SimplePermissions.isAdmin(Meteor.user());
    }
});