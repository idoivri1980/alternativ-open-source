Meteor.methods({
    addJourney: function (origin, destination, alternatives, chosenRoute, chosenType) {
        var journeyData = {
            origin: origin,
            destination: destination,
            alternatives: alternatives,
            chosenType: chosenType,
            chosenRoute: chosenRoute,
            userId: Meteor.userId(),
            createdAt: new Date()
        };
        var journeyId = Journeys.insert(journeyData);
        return journeyId;
    },
    export: function () {
        if (Meteor.userId() && SimplePermissions.isAdmin(Meteor.users.findOne(Meteor.userId()))) {
            var journeys = Journeys.find({}, {transform: addTracks}).fetch();
            return journeys;
        }
    },
    getPubJourney: function (id) {
        var journey = Journeys.findOne({_id: id});
        if (!journey) return;
        var chosenAlternative = null;
        journey.alternatives.forEach(function(item) {
            if (item.key === journey.chosenType) {
                chosenAlternative = item;
            }
        });
        var pubJourney = {
            chosenType: journey.chosenType,
            origin: journey.origin.address,
            destination: journey.destination.address,
            chosenAlternative: chosenAlternative
        };
        return pubJourney;
    },
    getUserJourneys: function (id) {
        if (!id) {
            console.log("no such user!");
            return {};
        }
        var journeysArray;

        var returnAll = id === 'all' && SimplePermissions.isAdmin(Meteor.users.findOne(Meteor.userId()));

        if (returnAll) {
            journeysArray = Journeys.find().fetch();
        } else {
            journeysArray = Journeys.find({userId: Meteor.userId()}).fetch();
        }

        //console.log("journeysArray: ",journeysArray);
        //console.log("journeysArray[0].alternatives: ",journeysArray[0].alternatives);

        var chosenAlternatives = {
            "DRIVING": 0,
            "WALKING": 0,
            "BIKING": 0,
            "BUS": 0,
            "TEL-O-FUN": 0,
            "TRAIN": 0,
            "TAXI": 0
        };
        var calories = 0;
        var duration = 0;
        var emissions = 0;
        var price = 0;
        for (var journey in journeysArray) {
            //console.log("Chosen Alternative in journey: ",journeysArray[journey].chosenType);
            var chosenAlternative = journeysArray[journey].alternatives.filter(
              function (alternative) {
                  return journeysArray[journey].chosenType === alternative.key;
              });

            chosenAlternatives[journeysArray[journey].chosenType] += 1;

            if (!chosenAlternative) {
                console.log("could not find chosen alternative!");
            }
            else if (chosenAlternative.length > 1) {
                console.log("error - more than one chosen alternative!");
            }
            else {
                calories += chosenAlternative[0].calories;
                price += chosenAlternative[0].price;
                emissions += chosenAlternative[0].emissions;
                duration += chosenAlternative[0].duration;
            }

        }

        var retValue = {
            totalJourneysAmount: journeysArray.length,
            alternatives: chosenAlternatives,
            calories: calories,
            price: price,
            emissions: emissions,
            duration: duration

        };

        //console.log("retvalue",retValue);

        return retValue;


    },
    //export journeys that were created on currentDate - daysPeriod and later
    exportJourneysbyPeriod: function(currentDate,daysPeriod) {
        var journeys = Journeys.find({
            //$gte : (new Date((currentDate).getTime() - (daysPeriod * 24 * 60 * 60 * 1000)))
            "createdAt": {
                //get all journeys of the last 30 minutes
                $gte: (new Date((currentDate).getTime() - (daysPeriod * 24 * 60 * 60 * 1000)))
            }

        },
          {transform: addTracks}).fetch();

        return journeys;

    },
    exportJourneysToDropbox: function (currentDate, daysPeriod) {


        console.log("exporting data of the last "+ daysPeriod +" days to dropbox");
        var journeys = Meteor.call('exportJourneysbyPeriod', new Date(), daysPeriod, function(err, result) {
            //console.log("Got result!", result);
            if (err) {
                console.log("Got error during export!",err);
                return err;
            }
            else {

                var data = JSON.stringify(result);
                data = new Buffer(data);

                var newFile = new FS.File();

                newFile.attachData(data, {type: 'text/plain'}, function(error) {
                    if (error) throw error;
                    var date = new Date();
                    var dateStr = date.getFullYear()+"-"+date.getMonth()+"-"+date.getDay()+"-"+date.getHours()+"-"+date.getMinutes();
                    newFile.name(dateStr+'.json');

                    routesExport.insert(newFile);

                });
                return "success";
            }

        });

    },

    getJourneysStats: function() {
        var permitted = (Meteor.userId() && SimplePermissions.isAdmin(Meteor.users.findOne(Meteor.userId())));
        if (!permitted) return;

        var midnight = new Date();
        midnight.setHours(0,0,0,0);
        var minDate = new Date(midnight.getTime() - 24 * 60 * 50 * 60000);
        var journeys = Journeys.find({createdAt: {$gt: minDate}}).fetch();
        var data = _.map(journeys, function(value, key) {
            var date = value.createdAt;
            date.setHours(0,0,0,0);
           return {
               dateCreatedAt: date,
               userId: value.userId
           };
        });
        var countByDate = _.countBy(data, 'dateCreatedAt');
        var countByDateArray = Array();
        for (var property in countByDate) {
            countByDateArray.push({date: property, journeys: countByDate[property]});
        }
        return {
            data: data,
            countByDate: countByDateArray
        };
        /*return Journeys.aggregate(
            { $project : { day : { $day : "$createdAt" }, userId : "$userId"} },
            { $group : { _id : "$day" , "Total" : { $count : "$userId"}}},
            { $project : { Month : "$_id", "Total" : "$Total", "_id" : 0} }
        )*/
    },

    getJourneySummaries: function() {
        var permitted = (Meteor.userId() && SimplePermissions.isAdmin(Meteor.users.findOne(Meteor.userId())));
        if (!permitted) return;

        var midnight = new Date();
        midnight.setHours(0,0,0,0);
        var minDate = new Date(midnight.getTime() - 24 * 60 * 50 * 60000);
        var journeys = Journeys.find({createdAt: {$gt: minDate}}).fetch();
        var data = _.map(journeys, function(value, key) {
            var date = value.createdAt;
            date.setHours(0,0,0,0);
            var chosenAlternative = value.alternatives.filter(
                function (alternative) {
                    return value.chosenType === alternative.key;
                });
            return {
                dateCreatedAt: date,
                userId: value.userId,
                chosenAlternativeType: value.chosenType,
                calories: chosenAlternative[0].calories,
                duration: chosenAlternative[0].duration,
                emissions: chosenAlternative[0].emissions,
                price: chosenAlternative[0].price
            };
        });

        return data;
    },

    changeUserPassword: function (userId, newPassword) {
        var permitted = (Meteor.userId() && SimplePermissions.isAdmin(Meteor.users.findOne(Meteor.userId())));
        if (!permitted) throw new Error("Not Permitted");

        var SHA256 = Package.sha.SHA256;
        var NpmModuleBcrypt = Package['npm-bcrypt'].NpmModuleBcrypt;
        var bcrypt = NpmModuleBcrypt;
        var bcryptHash = Meteor.wrapAsync(bcrypt.hash);

        var _bcryptRounds = 10;

        var hashPassword = function (password) {
            password = SHA256(password);
            return bcryptHash(password, _bcryptRounds);
        };

        var hashed = hashPassword(newPassword);



        console.log('new password for ', userId, ' is ', hashed);

        Meteor.users.update(
            {_id: userId},
            {
                $set: {'services.password.bcrypt': hashed},
            }
        );
    }
});

addTracks = function(journey) {
    var trackingDuration = 20;
    var trackingShouldEndAt = new Date(journey.createdAt.getTime() + trackingDuration*60000);
    var tracks = Tracks.find(
            {
              journeyId: journey._id,
              'location.timestamp': { $lt: trackingShouldEndAt.toISOString() }
            },
            {fields: {'location': 1}}).fetch();
    journey.tracks = tracks;
    return journey;
};