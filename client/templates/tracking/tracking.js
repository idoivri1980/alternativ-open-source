Template.tracking.helpers({
    trackingEnabled: function() {
        return Meteor.isCordova;
    },
    trackingState: function() {
        return JSON.stringify(Session.get('trackingState'));
    }
});

Template.tracking.events({
    'click #btn-start': function(evt) {
        evt.preventDefault();
        GeoTracker.setTrackingTime(30);
        GeoTracker.start();
    },
    'click #btn-stop': function(evt) {
        evt.preventDefault();
        GeoTracker.stop();
    },
    'click #btn-setjourney': function(evt) {
        evt.preventDefault();
        var journeyId = 'RJ' + Math.floor(Math.random() * 1000);
        GeoTracker.setJourneyId(journeyId);
    },
    'click #btn-startjourney': function(evt) {
        evt.preventDefault();
        Meteor.call('addJourney', null, null, [], 'walking', function(error, result) {
            if (error) {
                console.log(error.stack);
                return;
            }
            var journeyId = result;
            GeoTracker.setJourneyId(journeyId);
        });
    },
    'click #btn-updatestate': function(evt) {
        evt.preventDefault();
        GeoTracker.getState(function (state) {
            Session.set('trackingState', state);
        });
    }
});