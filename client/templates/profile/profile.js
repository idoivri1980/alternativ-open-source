/**
 * Created by idoivri on 23/11/2015.
 */

var journeysArray = null;

Template.profile.onCreated(function() {
    Session.set('journeysReady', false);

    //console.log("current user:", Meteor.userId());


    Meteor.call("getUserJourneys", Meteor.userId(),function(error, result) {
        if (error) {
            console.log(error.stack);
            return;
        }
        //console.log("meteor call returned OK with result", result);
        journeysArray = result;
        Session.set('journeysReady', true);

    });
    //console.log("User aggregated journeys data:", journeysArray);

    this.subscribe('curUserProfile');
});

Template.profile.helpers({
    goToLogin: function() {
        Router.go("/");
    },
    statistics: function() {
        if (Session.get('journeysReady')) {
            return journeysArray;
        }
    }
});

Template.profile.events({
    'click #btn-logout': function () {
        if (confirm('Are you sure you wish you log out?')) {
            Accounts.logout();
        }
    }
});