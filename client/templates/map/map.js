/**
 * Created by idoivri on 07/12/2015.
 */
var map, icons, directionsService, directionsDisplay;

Template.map.onCreated(function() {
   Session.set("chosenRouteJourneyId", ""); 
});

Template.map.onRendered(function() {
  if (!GoogleMaps.loaded()) {
    Router.go("navigate");

  }
  else {

    Session.set("recalculateResults",false);
    _prepareMap();
  }
});

Template.map.helpers({
    title: function () {
        var name = Session.get("draw_transportation");
        return name.charAt(0).toUpperCase() + name.slice(1).toLowerCase();
    }
});

var _prepareMap = function() {


  /* TODO: remove this from here! */

  CustomMarker.prototype = new google.maps.OverlayView();

  CustomMarker.prototype.draw = function() {

    var self = this;

    var div = this.div;

    if (!div) {

      div = this.div = document.createElement('div');

      div.className = 'marker';

      div.style.position = 'absolute';
      div.style.left = '70px';
      div.style.top = '352px';
      div.style.zIndex = 355;

      var innerDiv = document.createElement('div');
      if (self.type=="WALKING") {
        innerDiv.innerHTML = '<div style="box-shadow: rgba(0, 0, 0, 0.6) 0px 2px 5px; line-height: 12px; border: 1px solid rgb(102, 85, 102); padding: 2px; font-size: 13px; font-weight: 400; font-family: Roboto, Arial, sans-serif; white-space: nowrap; border-radius: 3px 3px 0px; position: absolute; right: 0px; bottom: 0px; background: -webkit-linear-gradient(top, rgb(249, 249, 249) 0%, rgb(238, 238, 238) 52%, rgb(249, 249, 249) 52%, rgb(227, 227, 227) 63%, rgb(238, 238, 238) 63%, rgb(227, 227, 227) 100%);"><img src="'+this.iconPath+'" draggable="false" style="width: 16px; height: 16px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; vertical-align: top; position: relative;"><img src="https://maps.gstatic.com/mapfiles/tiph.png" draggable="false" style="-webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; position: absolute; right: -8px; top: 19px; width: 15px; height: 9px;"></div>'
      }
      else if (self.type=="TEL-O-FUN") {
        innerDiv.innerHTML = '<div style="box-shadow: rgba(0, 0, 0, 0.6) 0px 2px 5px; line-height: 12px; border: 1px solid rgb(102, 85, 102); padding: 2px; font-size: 13px; font-weight: 400; font-family: Roboto, Arial, sans-serif; white-space: nowrap; border-radius: 3px 3px 0px; position: absolute; right: 0px; bottom: 0px; background: -webkit-linear-gradient(top, rgb(249, 249, 249) 0%, rgb(238, 238, 238) 52%, rgb(249, 249, 249) 52%, rgb(227, 227, 227) 63%, rgb(238, 238, 238) 63%, rgb(227, 227, 227) 100%);"><img src="'+this.iconPath+'" draggable="false" style="width: 16px; height: 16px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; vertical-align: top; position: relative;"><img src="https://maps.gstatic.com/mapfiles/tiph.png" draggable="false" style="-webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; position: absolute; right: -8px; top: 19px; width: 15px; height: 9px;"></div>'
      }
      div.appendChild(innerDiv);

      //if (typeof(self.args.marker_id) !== 'undefined') {
      //  div.dataset.marker_id = self.args.marker_id;
      //}

      google.maps.event.addDomListener(div, "click", function(event) {
        google.maps.event.trigger(self, "click");
      });

      var panes = this.getPanes();
      panes.overlayImage.appendChild(div);
    }

    var point = this.getProjection().fromLatLngToDivPixel(this.latlng);

    if (point) {
      div.style.left = point.x -6 + 'px';
      div.style.top = point.y -5 + 'px';
    }
  };

  CustomMarker.prototype.remove = function() {
    if (this.div) {
      this.div.parentNode.removeChild(this.div);
      this.div = null;
    }
  };

  CustomMarker.prototype.getPosition = function() {
    return this.latlng;
  };

  /* END TODO */


  var mapOptions = {
    zoom: 14,
    center: new google.maps.LatLng(TLV_UNIVERSITY_GEOCOORDS[0],TLV_UNIVERSITY_GEOCOORDS[1])
  };

  map = new google.maps.Map(document.getElementById("appmap"), mapOptions);

  addChooseRouteControl(map);

  directionsService = new google.maps.DirectionsService();
  directionsDisplay = new google.maps.DirectionsRenderer();
  directionsDisplay.setMap(map);

  var transportationToDraw = Session.get("draw_transportation");
  console.log("draw this:",transportationToDraw);

  drawOnMap(map, transportationToDraw);


};

var drawOnMap = function(map, transportationToDraw) {
  if (!map || !directionsDisplay || !transportationToDraw) {
    console.log("Can't draw on map, one or more needed elements is missing!");
  }
  var chosenRoute = [];
  var markers = [];
  console.log("Drawing method: ",transportationToDraw);
  var routesToDraw = resultsToDraw[transportationToDraw];

  //specifically for bus, do something else!
  if (transportationToDraw=="BUS" && routesToDraw.length>1) {
      console.log("Got multiple routes for a bus, drawing only the first one");
      console.log(routesToDraw[0]);

      var directionsRenderer = new google.maps.DirectionsRenderer;
      directionsRenderer.setMap(map);
      directionsRenderer.setDirections(routesToDraw[0]);
      //TODO: add control ui to choose a specific route or all, and don't display and don't set chosenRoute those that don't appear
      chosenRoute.push(routesToDraw[0]);
  }
  else if (transportationToDraw=="TEL-O-FUN") {
      for (route in routesToDraw) {
          console.log("DRAWING ROUTE# ",route);
          //console.log(routesToDraw[route]);
          if (routesToDraw[route].type=="WALKING") {
              var rendererOptions = getRendererOptions("WALKING"); //true is for walking
              var directionsRenderer = new google.maps.DirectionsRenderer(rendererOptions);
              //console.log("routesToDraw[route]",routesToDraw[route]);
              var leg = routesToDraw[route].routes[ 0 ].legs[ 0 ];

              var customWalkingIcon = new CustomMarker(leg.start_location, '/icons/transportation/walking_map_icon.png', routesToDraw[route].type ,map);

              directionsRenderer.setMap(map);
              //alterMapIcons(transportationToDraw, routesToDraw);
              //console.log("routes to draw", routesToDraw);
              markers.push(routesToDraw[route].request.origin);
              markers.push(routesToDraw[route].request.destination)
              chosenRoute.push(resultsToDraw[transportationToDraw][route]);
              directionsRenderer.setDirections(resultsToDraw[transportationToDraw][route]);
          }
          else {
              var rendererOptions = getRendererOptions("BIKING");
              var directionsRenderer = new google.maps.DirectionsRenderer(rendererOptions);
              var leg = routesToDraw[route].routes[ 0 ].legs[ 0 ];
              var otherCustomMarker = new CustomMarker(leg.start_location, '/icons/transportation/biking_map_icon.png', "TEL-O-FUN" ,map);

            directionsRenderer.setMap(map);
              markers.push(routesToDraw[route].request.origin);
              markers.push(routesToDraw[route].request.destination);
              chosenRoute.push(resultsToDraw[transportationToDraw][route]);
              directionsRenderer.setDirections(resultsToDraw[transportationToDraw][route]);

          }


      }
  }
  else {
    for (route in routesToDraw) {
      //console.log("DRAWING ROUTE# ",route);
      var rendererOptions = getRendererOptions(transportationToDraw);
      var directionsRenderer = new google.maps.DirectionsRenderer(rendererOptions);
      directionsRenderer.setMap(map);

      if (transportationToDraw=="WALKING") { //add walking icon ONLY to walking routes

        var leg = routesToDraw[route].routes[0].legs[0];
        var customWalkingIcon = new CustomMarker(leg.start_location, '/icons/transportation/walking_map_icon.png', transportationToDraw ,map);
      }


      console.log("routes to draw", routesToDraw);
      markers.push(routesToDraw[route].request.origin);
      markers.push(routesToDraw[route].request.destination)
      chosenRoute.push(resultsToDraw[transportationToDraw][route]);
      directionsRenderer.setDirections(resultsToDraw[transportationToDraw][route]);

    }
  }

  fitMapBoundsOfMarkers(map,markers);

  Session.set("chosenRoute",chosenRoute);

}

var fitMapBoundsOfMarkers = function(map , markers) {
  var markerBounds = new google.maps.LatLngBounds();

  for (var i = 0; i < markers.length; i++) {
    markerBounds = markerBounds.extend(markers[i]);
  }
  console.log("markers:",markers);
  map.setCenter(markerBounds.getCenter());
  map.fitBounds(markerBounds);
}

var drawCallBack = function(result, status) {

  //TODO: insert nav to DB ? only on committed stuff
  //var navRecord = {
  //  date: new Date,
  //  to: Session.get('to'),
  //  from: Session.get('from'),
  //  distance: distance,
  //  duration: duration,
  //  transType: Session.get('chosen').name,
  //  route: route,
  //  searchId: Session.get('search-id'),
  //  searchCraitiria: Session.get('sort-by')
  //};

  //console.log(navRecord);
  //Navigations.insert(navRecord);

  if (status == google.maps.DirectionsStatus.OK) {
    directionsDisplay.setDirections(result);
  }

}

var chooseRoute = function () {
    var method = Session.get("draw_transportation");
    console.log("transportation medhod "+method+" was chosen! Starting GPS logging");

    var alternatives = Session.get("results");
    alternatives.forEach(function (item) {
        delete item.icon;
        delete item.type;
    });
    var origin = Session.get("currentFromLocation");
    var destination = Session.get("currentToLocation");

    var chosenRoute = Session.get("chosenRoute");

    if (!alternatives || !origin || !destination || !chosenRoute || chosenRoute.length==0) {
      console.log("one or more critical objects is missing in order to start GPS logging");
      console.log(alternatives,origin,destination, chosenRoute);
    }
    Meteor.call("addJourney", origin, destination, alternatives, chosenRoute, method, function (err, result) {
      if (err) {
        console.log("GPS Error");
        console.log(err.stack);
      }
      else {
        //start GPS tracking with the result = journey ID
        GeoTracker.stop();
        GeoTracker.setJourneyId(result);
        GeoTracker.setTrackingTime(30);
        GeoTracker.start();
        Session.set("chosenRouteJourneyId", result);
      }
      
      Session.set("hasChosenRoute", true);
    });
    ga("send", "event", "navigation", "choose-route", method);
};


var addChooseRouteControl = function(map) {
  var centerControlDiv = document.createElement('div');
  var controlUI = document.createElement('div');
  controlUI.classList.add('btn');
  controlUI.classList.add('btn-primary');
  controlUI.classList.add('btn-choose-route');
  controlUI.title = 'Choose this transportation method';
  controlUI.innerText = 'Choose this route!';
  centerControlDiv.appendChild(controlUI);

  controlUI.addEventListener('click', function() {
    chooseRoute();
    controlUI.style.display = 'none';
    addShareControl(map);
  });
  centerControlDiv.index = 1;
  map.controls[google.maps.ControlPosition.BOTTOM_CENTER].push(centerControlDiv);
};


var addShareControl = function (map) {
  var centerControlDiv = document.createElement('div');
  var controlUI = document.createElement('div');
  
  var controlUI = $(
        '<div class="alert alert-info">' +
          '<span class="message">Thank you!</span>' + 
          '<a class="btn btn-facebook btn-share-route"><span class="icon-facebook"></span> Share</a>' + 
        '</div>')[0];
  
  centerControlDiv.appendChild(controlUI);

  controlUI.addEventListener('click', function() {
    var journeyId = Session.get("chosenRouteJourneyId");
    if (journeyId === '') {
        alert('Sorry, sharing to Facebook failed.');
        return;
    }
    var shareUrl = Meteor.absoluteUrl('/journey/' + journeyId);
    FB.ui(
       {
         method: 'share',
         href: shareUrl
       }, function(response){
           if (!response.error_code) {
               jQuery(controlUI).find('.btn-share-route').hide();
               jQuery(controlUI).find('.message').append(' <span class="glyphicon glyphicon-ok"></span> <strong>Shared</strong>');
           }
       });
  });
  
  centerControlDiv.index = 1;
  map.controls[google.maps.ControlPosition.BOTTOM_CENTER].push(centerControlDiv);

  setTimeout(function() {
    controlUI.classList.add('visible');
  }, 100);
};


var getRendererOptions = function (rendererType) {
  var rendererOptions = {};

  //display "dotted" walking only if park type is shuttle
  if (rendererType=="WALKING") {

    var icon = {
      fillColor: 'blue',
      fillOpacity: 0.5,
      scale: 3,
      strokeColor: 'blue',
      strokeWeight: 1,
      strokeOpacity: 0.8,
      path: google.maps.SymbolPath.CIRCLE
    };

    var polyLineOptions = {
      strokeOpacity: 0,
      icons: [{
        icon: icon,
        offset: '0',
        repeat: '10px'
      }]
    };

    rendererOptions = {

        polylineOptions: polyLineOptions,
        preserveViewport: true
    };

    if (//rendererType=="WALKING" ||
         rendererType=="BIKING") {
        //rendererOptions.suppressMarkers= true;
    }
  }
  return rendererOptions;
};

var getTelOFunMarkerDescription = function (direction,station) {

  var retString = '<div style="display: inline-block; overflow: auto; max-height: 718px; max-width: 300px;">'+
    '<div class="gm-iw gm-transit" style="max-width: 200px; jstcache="0">'+
    '<img style="margin-left: -15px; margin-right: 5px; width: 20px; height: 20px; border: 0px 10px 0px 0px;" jsdisplay="$icon" jsvalues=".src:$icon" jstcache="1" src="images/icons/bicycle_000000_20.png">'+
    '<div jsvalues=".innerHTML:$this.instructions" jstcache="2">';

    retString+= (direction=="from"? 'Take ':'Return ' )+'a bike '+ (direction=="from"? "from ":"at ")+"station: ";

    retString+="<strong>"+station.stationName+'</strong>.</div></div></div>';

  return retString;
}

var myStyle = [{"featureType":"administrative","elementType":"all","stylers":[{"visibility":"on"},{"lightness":33}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2e5d4"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#c5dac6"}]},{"featureType":"poi.park","elementType":"labels","stylers":[{"visibility":"on"},{"lightness":20}]},{"featureType":"road","elementType":"all","stylers":[{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"color":"#c5c6c6"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#e4d7c6"}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#fbfaf7"}]},{"featureType":"water","elementType":"all","stylers":[{"visibility":"on"},{"color":"#acbcc9"}]}];

function CustomMarker(latlng, iconPath, type,  map) {
  this.latlng = latlng;
  this.type = type;
  this.iconPath = iconPath;
  this.setMap(map);
}