Template.notifications.events({
    'submit form': function (evt) {
        evt.preventDefault();
        if (confirm('Are you sure?')) {
            var title = $('#push-title').val();
            var text = $('#push-text').val();
            Push.send({
                from: 'AlterNativ',
                title: title,
                text: text,
                query: {}
            });
            alert('Pushed!');
            $('#push-title').val('');
            $('#push-text').val('');
        }
    }
});