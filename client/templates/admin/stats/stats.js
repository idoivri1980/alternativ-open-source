Template.stats.helpers({
    data: function() {
        return Session.get('statsData');
    },
    journeysTotals: function() {
        return Session.get('journeysTotals');
    }
});

Template.stats.onCreated(function() {
    Meteor.call('getJourneysStats', function(err, result) {
        if (err) return;
        Session.set('statsData', result);
    });
    Meteor.call('getUserJourneys', 'all', function(err, result) {
        if (err) return;
        Session.set('journeysTotals', result);
    });
});