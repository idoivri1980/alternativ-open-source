Template.users.created = function () {
    this.subscribe('users');
};

Template.users.helpers({
    users: function () {
        return Meteor.users.find();
    }
});

Template.users.events({
    'click .btn-dropbox': function (evt) {
        console.log("Dropbox button pushed!");
        Meteor.call('exportJourneysToDropbox', new Date(), 7, function(err, result) {
            if (err) console.log("error!",err);
            else console.log("wrote to Dropbox OK!");
        });
    }
});