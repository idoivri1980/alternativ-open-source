resultsToDraw = {
    "DRIVING": [],
    "WALKING": [],
    "BIKING": [],
    "BUS": [],
    "TEL-O-FUN": [],
    "TRAIN": [],
    "TAXI": []
};


Meteor.startup(function () {

    Session.set("recalculateResults",true);

    console.log("loading GoogleMaps: geometry, places, directions");
    var userLanguage = navigator.language;
    if (userLanguage !== 'he-IL') userLanguage = 'en-US';
    GoogleMaps.load({libraries: 'geometry,places,directions', language:userLanguage});

    if (Meteor.isCordova) {
        GeoTracker.init();
        GeoTracker.updateFromUser(Meteor.user());
        Accounts.onLogin(function () {
            console.log('onLogin');
            GeoTracker.updateFromUser(Meteor.user());
        });
        document.addEventListener("backbutton", function(){
            if ($('#nav-drawer').hasClass('open')) {
                $('#nav-drawer').drawer('hide');
            } else if (history.state && history.state.initial === true) {
                navigator.app.exitApp();
            } else {
                history.go(-1);
            }
        });
    }

    Accounts.onLogin(function () {
        // update isAdmin
        Meteor.call('isAdmin', function (err, result) {
            if (!err) Session.set('isAdmin', result);
        });
        // redirect to navigation
        if (Router.current() && Router.current().route.getName() === "login"){
            Router.go("/navigate");
        }
    });
    
    /*Accounts.ui.config({
        requestPermissions: {
            facebook: ['publish_actions']
        }
    });*/
    
    Meteor.Spinner.options.color = '#fff';
    
});

var facebookConfig = Meteor.settings.public.facebook;
if (facebookConfig) {
    window.fbAsyncInit = function() {
        console.log('fbAsyncInit');
        FB.init({
          appId      : facebookConfig.appId,
          version    : 'v2.4'
        });
    };
}